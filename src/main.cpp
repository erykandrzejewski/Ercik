#include <Ercik/Bot.hpp>
#include <Ercik/Server.hpp>
#include <iostream>

int main()
{
  ee::Server s1("irc.freenode.net", 6667);
  s1.addChannel("#testchannel1");
  s1.addChannel("#testchannel2");
  s1.addChannel("#testchannel3");

  ee::Server s2("irc.swiftirc.net", 6667);
  s2.addChannel("#testchannel1");
  s2.addChannel("#testchannel2");
  s2.addChannel("#testchannel3");

  ee::Bot bot("Ercik");
  bot.addServer(s1);
  bot.addServer(s2);
  bot.run();
}
