#include <Ercik/IrcSocketSfml.hpp>
#include <Ercik/IrcSocketException.hpp>
#include <iostream>

namespace ee
{
  IrcSocketSfml::IrcSocketSfml() :
  socket(new sf::TcpSocket)
  {
  }

  void IrcSocketSfml::connect(const std::string& server_address, const int port)
  {
    sf::Socket::Status status = socket->connect(server_address, port);

    if (status != sf::Socket::Done)
      throw ee::IrcSocketConnectException("Couldn\'t connect to the server");

    socket->setBlocking(false);
  }

  void IrcSocketSfml::disconnect()
  {
    socket->disconnect();
  }

  void IrcSocketSfml::sendData(std::string message)
  {
    message += "\n\r";
    std::size_t send_bytes;
    sf::Socket::Status status = socket->send(message.c_str(), message.size(), send_bytes);

    if (status != sf::Socket::Done || send_bytes != message.size())
      throw ee::IrcSocketDataSendException("Data send error");
  }

  std::string IrcSocketSfml::receiveData()
  {
    std::string buffer(MAX_MESSAGE_SIZE, '\0');
    std::size_t received_bytes = 0;

    sf::Socket::Status status = socket->receive(&buffer[0], MAX_MESSAGE_SIZE, received_bytes);

    if (status == sf::Socket::NotReady)
      return std::string("");

    if (status != sf::Socket::Done)
      throw ee::IrcSocketDataReceiveException("There was an error in data transmission");

    return buffer.substr(0, received_bytes);
  }
}
