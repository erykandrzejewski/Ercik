CXX = g++
CXXFLAGS = -std=c++11 -Iinclude
LD = g++
LDFLAGS = -lsfml-network -lsfml-system

OBJS = obj/main.o obj/Bot.o obj/Server.o obj/Plugin.o obj/IrcSocketSfml.o obj/IrcMessage.o

all: ercik
ercik: $(OBJS)
	$(LD) -o $@ $^ $(LDFLAGS) $(CXXFLAGS)

obj/%.o: src/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

setup:
	mkdir obj

clean:
	rm -r obj/*
	rm ercik

run:
	./ercik
