#ifndef IRC_SOCKET_HPP
#define IRC_SOCKET_HPP

#include <string>

namespace ee
{
  class IrcSocket
  {
  public:
    IrcSocket()
    {
    }

    virtual void connect(const std::string& server_address, const int port) = 0;
    virtual void disconnect() = 0;

    virtual void sendData(std::string message) = 0;
    virtual std::string receiveData() = 0;
  };
}

#endif
