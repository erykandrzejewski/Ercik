#ifndef BOT_HPP
#define BOT_HPP

#include <Ercik/Server.hpp>
#include <Ercik/Plugin.hpp>
#include <Ercik/IrcSocket.hpp>

#include <vector>
#include <string>
#include <memory>

namespace ee
{
	class Bot
	{
	public:
		Bot(const std::string& nickname);
		void setNickname(const std::string& nickname);

		void addServer(ee::Server& server);
		void removeServer(ee::Server& server);

		void addPlugin(ee::Plugin& plugin);
		void run();

	private:
		void initialize();

		std::string nickname;

		std::vector<ee::Server> servers_list;
		std::vector<ee::Plugin> plugins_list;
		std::vector<std::unique_ptr<ee::IrcSocket>> sockets_list;
	};
}

#endif
