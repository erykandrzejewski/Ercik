#ifndef SERVER_HPP
#define SERVER_HPP

#include <string>
#include <vector>

namespace ee
{
  class Server
  {
  public:
    Server(const std::string& server_address, const int server_port);

    void setAddress(const std::string& server_address);
    void setPort(const int server_port);

    std::string getAddress();
    int getPort();

    void addChannel(const std::string& channel);
    void removeChannel(const std::string& channel);
    std::vector<std::string> getChannelsList();

    bool operator==(const ee::Server& server);

  private:
    std::string server_address;
    int server_port;

    std::vector<std::string> channels_list;
  };
}

#endif
