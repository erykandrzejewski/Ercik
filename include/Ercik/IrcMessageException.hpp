#ifndef IRC_MESSAGE_EXCEPTION_HPP
#define IRC_MESSAGE_EXCEPTION_HPP

#include <string>

namespace ee
{
  class IrcMessageException
  {
  public:
    IrcMessageException(const std::string& message)
    {
      this->message = message;
    }

    std::string what() const
    {
      return message;
    }

  private:
    std::string message;
  };
}

#endif
