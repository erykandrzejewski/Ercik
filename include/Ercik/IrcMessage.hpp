#ifndef IRC_MESSAGE_HPP
#define IRC_MESSAGE_HPP

#include <string>
#include <vector>

namespace ee
{
  class IrcMessage
  {
  public:
    enum class SenderType {None, Client, Server};

    IrcMessage();

    // Methods for messages that came from Server
    void setServerMessage(const std::string& raw_message);

    bool hasPrefix();
    bool isServerTheSender();
    std::string getServerName();
    std::string getSenderNickname();
    std::string getSenderUsername();
    std::string getSenderHostname();

    // Metods for messages that were send by client

    std::string getClientMessage();
    void setCommand(const std::string& command);
    void addCommandParameter(const std::string& parameter);

    // Methods for free usage

    std::string getCommand();
    std::vector<std::string> getCommandParametersList();
    std::string getCommandParameter(int index);

  private:
    SenderType sender_type;

    // Prefix
    bool has_prefix;
    bool is_server_the_sender; // If the sender is server, then prefix is server_name.
    std::string server_name;
    std::string sender_nickname;
    std::string sender_username;
    std::string sender_hostname;

    // Command and command parameters
    std::string command;
    std::vector<std::string> command_parameters_list;
  };
}

#endif
