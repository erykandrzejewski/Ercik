#ifndef IRC_SOCKET_SFML_HPP
#define IRC_SOCKET_SFML_HPP

#include <Ercik/IrcSocket.hpp>
#include <SFML/Network.hpp>
#include <string>
#include <memory>

namespace ee
{
  class IrcSocketSfml : public IrcSocket
  {
  public:
    IrcSocketSfml();

    void connect(const std::string& server_address, int port);
    void disconnect();

    void sendData(std::string message);
    std::string receiveData();

  private:
    std::unique_ptr<sf::TcpSocket> socket;
    const int MAX_MESSAGE_SIZE = 1024;
  };
}

#endif
